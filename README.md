# Cloning, renaming, and creating new repo
In the steps below, replace ``new-poster`` with an identifier for the paper you're
writing.  For consistency with other paper repos, make it descriptive, lowercase,
and with hyphens separating words; e.g., cross-modal-plans-2020.

    git clone git@gitlab.com:wenkaiqin/poster-template.git new-poster
    cd new-poster
    git remote rename origin old-origin
    rm README.md
    git push --set-upstream git@gitlab.com:yourname/new-poster.git main
    git remote add origin git@gitlab.com:yourname/new-poster.git

Do not rename ``poster.tex``:  the directory name provides
enough unique identification, and it's easier to compile and work with papers if
they're all named ``poster.tex`` and ``poster.pdf``, etc.
